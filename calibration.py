import sys
import os

times_to_run = 5

if len(sys.argv) < 3:
    print("usage python calibration.py <input_folder> <output_folder>")
    exit()

temps  = [125, 250, 500]
alphas = [0.5, 0.7, 0.9]
iterations = [1000, 5000, 20000]


for filename in os.listdir(sys.argv[1]):
    if not os.path.exists(sys.argv[2]):
        os.makedirs(sys.argv[2])

    for t in range (0, len(temps)):
        for a in range (0, len(alphas)):
            for it in range (0, len(iterations)):
                arguments = "-t {0} -a {1} -i {2}".format(temps[t], alphas[a], iterations[it])
                filename_out = str(t) + "-" + str(a) + "-" + str(it) + ".txt"
                sys_call = "echo {0} >> {1}".format(arguments, sys.argv[2]+"/"+filename);
                os.system(sys_call)
                sys_call = "echo ------------ >> {0}".format(sys.argv[2]+"/"+filename);
                os.system(sys_call)
                for i in range(0, times_to_run):
                    sys_call = "./bin/main out.txt {0} < {1} >> {2}".format(arguments, sys.argv[1]+"/"+filename, sys.argv[2]+"/"+filename)
                    os.system(sys_call)
