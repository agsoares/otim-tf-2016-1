param m, integer, > 0;
param n, integer, > 0;

set W{1..n};

set S{1..m};

var x{1..n}, binary;

s.t. c{i in 1..m}: sum{j in S[i]} (x[j]) >= 1;

minimize z: sum{i in 1..n, j in W[i]} x[i] * j;

solve;

printf '%s of %g \n', sum{i in 1..n} x[i], sum{i in 1..n, j in W[i]} j;

end;
