import sys
import os
from collections import deque

if len(sys.argv) < 3:
    print("usage python parser.py <input_folder> <output_folder>")
    exit()


for filename in os.listdir(sys.argv[1]):
    m = 0
    n = 0
    S = []
    W = []
    file_in = open(sys.argv[1] + "/" + filename, "r")

    s = file_in.read().replace("\n", "")
    values = deque(s.strip().split())
    m = int(values.popleft())
    n = int(values.popleft())

    for i in range(0, n):
        w = values.popleft()
        W.append(w)

    for i in range(0, m):
        k = int(values.popleft())
        S.append([])
        for j in range(0, k):
            S[i].append(int(values.popleft()))

    file_in.close()
    file_out = open(sys.argv[2] + "/" + filename, "w")
    file_out.write("data;\n")
    file_out.write("param m := {0};\n".format(m))
    file_out.write("param n := {0};\n".format(n))
    for i in range(0,len(W)):
        file_out.write("set W[{0}] := {1};\n".format(i+1, W[i]))

    for i in range(0,len(S)):
        file_out.write("set S[{0}] := {1};\n".format(i+1, ', '.join(str(x) for x in S[i])))

    file_out.write("end;\n\n")
    file_out.close()
