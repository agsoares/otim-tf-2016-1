import sys
import os

times_to_run = 5
temp  = 240
alpha = 0.9
it    = 20000

if len(sys.argv) < 3:
    print("usage python meta.py <input_folder> <output_folder>")
    exit()

arguments = "out.txt -t {0} -a {1} -i {2}".format(temp, alpha, it)

for filename in os.listdir(sys.argv[1]):
    if not os.path.exists(sys.argv[2]):
        os.makedirs(sys.argv[2])

    for i in range(0, times_to_run):
        sys_call = "./bin/main {0} < {1} >> {2}".format(arguments, sys.argv[1]+"/"+filename, sys.argv[2]+"/"+filename)
        os.system(sys_call)
