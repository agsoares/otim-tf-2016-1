#include <cmath>
#include <climits>
#include <cstdio>
#include <cstdlib>

#include <cstring>
#include <iostream>
#include <fstream>
#include <ctime>

#include <vector>
#include <random>
#include <functional>

using namespace std;

typedef vector<int>  vi;
typedef vector<pair <bool, int> > vb;

int n = 0; int m = 0;

vb vars;
vector <vi> sets;
vi counters;

int best;
double best_time;
int actual_best;

int fitness(vb vars) {
    int count = 0;

    for (int i = 0; i < vars.size(); i++) {
        if (vars[i].first) count += vars[i].second;
    }
    return count;
}

bool feasible (int x, vb vars) {
    if (vars[x].first) return true;
    for (int i = 0; i < sets[x].size(); i++) {
        if (counters[sets[x][i]] == 1) {
            return false;
        }
    }
    return true;
}

void recalculate (int x, vb vars) {
    for (int i = 0; i < sets[x].size(); i++) {
        counters[sets[x][i]] += (vars[x].first) ? 1 : -1;
    }
}

int main(int argc, char** argv) {
    srand(time(NULL));
    double start_time = time(NULL);


    double initial_temp = 120.0;
    double stop_temp = 0.00008;
    double cooling = 0.9;
    const double e = 2.718281828;

    int max_iterations = 20000;

    ofstream file;
    file.open(argv[1], ios::out);

    //ARGS PARSING
    for (int a = 2; a < argc; a++) {
        if (strcmp(argv[a], "-t") == 0) {
            initial_temp = atof(argv[++a]);
        }
        if (strcmp(argv[a], "-a") == 0) {
            cooling = atof(argv[++a]);
        }
        if (strcmp(argv[a], "-it") == 0) {
            max_iterations = atoi(argv[++a]);
        }
    }


    //INPUT PARSING
    cin >> m >> n;
    default_random_engine generator;
    uniform_int_distribution<int>     distribution(0,n-1);
    uniform_real_distribution<double> real_distribution(0,1);
    vars.assign(n, make_pair(false, 0));
    counters.assign(m, 0);
    sets.assign(m, vi());
    for (int i = 0; i < n; i++) {
        int x;
        cin >> x;
        vars[i].first = true;
        vars[i].second = x;
    }
    for (int i = 0; i < m; i++) {
        int s;
        cin >> s;
        for (int j = 0; j < s; j++) {
            int k;
            cin >> k;
            sets[--k].push_back(i);
            if (vars[k].first)
                counters[i] += 1;
        }

    }


    //SIMULATED ANNEALING
    actual_best = fitness(vars);
    best = actual_best;
    cout  << "Initial Solution: " << best << endl;
    for (double T = initial_temp; T > stop_temp; T *= cooling) {
        for (int i = 0; i < max_iterations; i++) {
            int flip;

            flip = distribution(generator);//(rand()%n);
            vars[flip].first = !vars[flip].first;

            int fit;

            if (feasible(flip, vars)) {
                fit = fitness(vars);
            }
            else {
                fit = INT_MAX;
            }

            double prob  = rand()/(double)RAND_MAX;
            double delta = pow(e, (double)(best-fit)/T);

            if (fit <= actual_best || prob < delta) {
                actual_best = fit;
                if (actual_best < best) {
                    best = actual_best;
                    best_time = (time(NULL) - start_time);
                    //cout  << "New Best: " << best << endl;
                }
                recalculate(flip, vars);

            } else {
                vars[flip].first = !vars[flip].first;
            }
        }
    }
    cout << "Best Solution: " << best;
    cout << " of " << n << endl;
    cout << "Time: " << difftime(time(NULL), start_time) << " seconds" << endl << endl;

    for (int i = 0; i < n; i++) {
        if (i > 0 && i%12 ==0) file << endl;
        file << " " << vars[i].first;

    }
    file << endl;
    file.close();

    return 0;
}
